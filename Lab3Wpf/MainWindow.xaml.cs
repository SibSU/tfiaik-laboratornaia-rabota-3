﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lab3Wpf
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<Node> tree;
        

        Random random;
        Logging logging;
        List<History> histories;

        Dictionary<char, Dictionary<int, string>> Commands;
        string Start, Input, Final;

        public MainWindow()
        {
            InitializeComponent();

            tree = new ObservableCollection<Node>();
            treeView1.ItemsSource = tree;

            random = new Random();
            logging = new Logging(textLog);
            histories = new List<History>();

            textCommand.Text = "A1AB\r\nA2ABC\r\nA3C\r\nB1A\r\nB2B\r\nB3C";
            textZ.Text = "123";
            textQ.Text = "ABC";
            textStart.Text = "B";
            textFinal.Text = "ABC";
            textInput.Text = "12213";

            btnStart.Click += BtnStart_Click;
        }

        private void BtnStart_Click(object sender, RoutedEventArgs e)
        {
            tree.Clear();
            Commands = InitialCommand(textCommand.Text, textZ.Text, textQ.Text);
            Start = textStart.Text;
            Input = textInput.Text;
            Final = textFinal.Text;

            Node head = new Node("");
            tree.Add(head);
            Solve(head, Start, Input);
        }

        private void Solve(Node node, string startP, string input, string old = "")
        {
            char start = startP[0];


            // Проверяем есть ли еще на входе команды
            if (input.Length > 0)
            {
                node.Name += start;

                Dictionary<int, string> temp;

                // Проверяем есть ли у состояния какие-то команды 
                if (Commands.TryGetValue(start, out temp))
                {
                    node.Name += " - " + input[0] + " -> ";

                    String state;

                    if (temp.TryGetValue(Int32.Parse(input[0].ToString()), out state))
                    {
                        // Проверяем сколько состояний
                        if (state.Length == 1)
                        {
                            node.Name += state[0];

                            Node child = new Node("", node);
                            node.AddChild(child);
                            Solve(child, state[0].ToString(), input.Substring(1));
                        }
                        else
                        {
                            if (old.Length > 0)
                            {
                                foreach (char s in old)
                                {
                                    state = state.Replace(s.ToString(), String.Empty);
                                }
                            }

                            // Проверяем не обошли ли мы 
                            if (state.Length == 0)
                            {
                                // Проверяем есть ли что то в истории: если есть, то рекурсия
                                if (histories.Count > 0)
                                {
                                    node.Info = "все варианты данной пары оказались тупиковыми";

                                    History last = histories.Last();
                                    histories.Remove(last);

                                    Node child1 = new Node("", last.Node.Parent);
                                    last.Node.Parent.AddChild(child1);
                                    Solve(child1, last.Node.Name[0].ToString(), last.Input, last.Result);
                                }
                                else
                                {
                                    node.Info = "Варианты кончились, аварийная остановка";
                                }
                                return;
                            }

                            char newState = state[random.Next(state.Length)];
                            histories.Add(new History(node, input, newState + old));
                            node.Name += newState;
                            node.Info = " (возможные варианты " + state + ")";

                            Node child = new Node("", node);
                            node.AddChild(child);
                            Solve(child, newState.ToString(), input.Substring(1));
                        }
                    }
                    else
                    {
                        // Проверяем есть ли что то в истории: если есть, то рекурсия
                        if (histories.Count > 0)
                        {
                            node.Info = "тупик, нет команд для данной пары";

                            History last = histories.Last();
                            histories.Remove(last);

                            Node child = new Node("", last.Node.Parent);
                            last.Node.Parent.AddChild(child);
                            Solve(child, last.Node.Name[0].ToString(), last.Input, last.Result);
                        }
                        else
                        {
                            node.Info = "тупик, аварийная остановка";
                        }
                    }
                }
                else
                {
                    // Проверяем есть ли что то в истории: если есть, то рекурсия
                    if (histories.Count > 0)
                    {
                        node.Info = "тупик, нет команд для данного состояния";

                        History last = histories.Last();
                        histories.Remove(last);

                        Node child = new Node("", last.Node.Parent);
                        last.Node.Parent.AddChild(child);
                        Solve(child, last.Node.Name[0].ToString(), last.Input, last.Result);
                    }
                    else
                    {
                        node.Info = "тупик, аварийная остановка";
                    } 
                }

            }
            else
            {
                if (Final.Contains(start))
                {
                    node.Name += start;
                    node.Info = "Конечное состояние является штатным";
                } else
                {
                    // Проверяем есть ли что то в истории: если есть, то рекурсия
                    if (histories.Count > 0)
                    {
                        node.Name += start;
                        node.Info = "тупик, конечное состояние не является штатным";

                        History last = histories.Last();
                        histories.Remove(last);

                        Node child = new Node("", last.Node.Parent);
                        last.Node.Parent.AddChild(child);
                        Solve(child, last.Node.Name[0].ToString(), last.Input, last.Result);
                    }
                    else
                    {
                        node.Name += start;
                        node.Info = "Конечное состояние не является штатным";
                    }
                }
            }
        }

        /// <summary>
        /// Метод преобразует набор команд, представленный в виде строки, во внутреннее представление программы
        /// </summary>
        /// <param name="value">Набор команд в виде строки состоящей из троек</param>
        /// <param name="z">Алфавит Z</param>
        /// <param name="g">Алфавит Г</param>
        /// <returns>Набор комманд в виде словаря</returns>
        public Dictionary<char, Dictionary<int, string>> InitialCommand(string value, string z, string g)
        {
            Dictionary<char, Dictionary<int, string>> commands = new Dictionary<char, Dictionary<int, string>>();

            string[] temp = textCommand.Text.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < temp.Count(); i++)
            {
                if (temp[i].Length > 2 && g.Contains(temp[i][0]) && z.Contains(temp[i][1]))
                {
                    for (int j = 2; j < temp[i].Length; j++)
                    {
                        if (!g.Contains(temp[i][j])) throw new Exception("неверно задан набор команд.");
                    }

                    Dictionary<int, string> tempCommand;
                    if (commands.TryGetValue(temp[i][0], out tempCommand))
                    {
                        commands.Remove(temp[i][0]);
                    }
                    else
                    {
                        tempCommand = new Dictionary<int, string>();
                    }

                    int a = Int32.Parse(temp[i][1] + "");

                    // Удаление повторяющихся символов 
                    IEnumerable<char> res = temp[i].Substring(2).Distinct();
                    if (res.Count() != temp[i].Substring(2).Length)
                    {
                        string s = "";
                        foreach (char ch in res)
                        {
                            s += ch;
                        }
                        temp[i] = temp[i].Substring(0, 2) + s;
                    }
                    tempCommand.Add(a, temp[i].Substring(2));


                    commands.Add(temp[i][0], tempCommand);
                }
                else
                {
                    throw new Exception("неверно задан набор команд.");
                }
            }

            textCommand.Text = "";
            foreach (string row in temp)
            {
                textCommand.Text += row + "\r\n";
            }

            return commands;
        }

    }
}
