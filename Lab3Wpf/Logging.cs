﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Lab3Wpf
{
    class Logging
    {
        private TextBox textBox;

        public Logging(TextBox textBox)
        {
            this.textBox = textBox;
        }

        public void Clear()
        {
            this.textBox.Text = "";
        }

        public void Add(string text)
        {
            this.textBox.Text += text;
        }

        public void AddLine(string text)
        {
            Add(text);
            Add("\n");
        }
    }
}
