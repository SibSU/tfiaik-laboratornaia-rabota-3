﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3Wpf
{
    class History
    {
        public Node Node;

        public string Input;

        public string Result;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="node">Вершина дерева</param>
        /// <param name="input">Входная цепочка команд</param>
        /// <param name="result">Результирующее состояние</param>
        public History(Node node, string input, string result)
        {
            this.Node = node;
            this.Input = input;
            this.Result = result;
        }
    }
}
